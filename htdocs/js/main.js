(function(){
    //##########################################
    //############################## Service Worker Register ###########
	if('serviceWorker'in navigator){
		//console.log(navigator)
		navigator.serviceWorker.register('./service-worker.js',{scope:'./'})
		.then((reg)=>{
           console.log("Service-Worker arbeitet");
		}).catch((error)=>{
			console.log('Registration Fehler:'+ error);
		})

	};
    //######################## ENDE service Worker Register###########
    //################################################

    //******************************************************/
    //******************** G L O B A L  ********************/
    //******************************************************/

    // Abteilung Reservierungssystem

    let countSelected = 0;          // Anzahl ausgewählter Sitzplätze 

    let kino,film,zeit,data,nameKino,nameFilm,uhrzeit; 

    let preis;                      // Einzelpreis pro Sitzplatz des ausgewälten Films
    let anzahl = 0;
    let storageData = {};           // leeres Objekt für storage

    // Objekt mit Methoden zum Speichern, Lesen und Löschen von Objekten im Local Storage
    // aus Funstepper ;)

    let storage = {             
		read: function(key){
			let data = localStorage.getItem(key);
			if(!data){return {};} // Wenn Speicher leer
			storageData = JSON.parse(data);
			return storageData;
		},
		clear:function(){
            // alle items im Local Storage werden gelöscht
			localStorage.clear();
        },
		save:function(key,data){
			let temp = JSON.stringify(data);
			localStorage.setItem(key,temp);
		}
    };
    // ENDE Abteilung Reservierungssystem

    // Abteilung Canvas
    let canvasCorona, ctx;
    let graphCanvas,graphCtx;
    let graph = [];
    let persons = new Array();
    let elapsedTime; // counter für Time 
    let animate = false;        // steuert die Animation (function render)

    // Hintergrundbilder für Canvas
    let imgCanvasCorona = new Image();
    imgCanvasCorona.src = 'img/CANVAS_kino_corona_dark.png';
    let imgCanvasGraph = new Image();
    imgCanvasGraph.src = 'img/Canvas_graph_background.png';

    // ENDE Abteilung Canvas

    
    //********************************************************************/
    //********************** Variablen und Funktionen ********************/
    //********************************************************************/

    // Datensatz aus JSON laden
    async function loadJson(){
        data = await (await fetch('data/kinos.json')).json();
        // console.log(data);

        // erstellt select-Element im DOM mit allen Kinos aus der JSON-Datei
        kinoAuswahl(data);

        // Rückgabe des gewählten Kinos, Speicherung in Variablen und Ausgabe im DOM
        el('#kino').addEventListener('change',function(){
            nameKino = this.options[this.selectedIndex].text;
            kino = this.options[this.selectedIndex].value;
            // console.log(kino + 'Name: ' + name);
            el('#ausgabe_wahl').innerHTML = nameKino;

            // erstellt select-Element im DOM mit allen Filmen des ausgewählten Kinos aus der JSON-Datei
            filmAuswahl(data,kino);

            // Rückgabe des gewählten Films, Speicherung in Variablen und Ausgabe im DOM
            el('#film').addEventListener('change',function(){
                nameFilm = this.options[this.selectedIndex].text;
                film = this.options[this.selectedIndex].value;
                // console.log(this.options)
                // console.log('Name: ' + name);
                el('#ausgabe_wahl').innerHTML += '<br>' + nameFilm;
                
                // erstellt select-Element im DOM mit allen Spielzeiten des ausgewählten Films im ausgewählten Kinos aus der JSON-Datei
                zeitAuswahl(data,kino,film);

                // Rückgabe der gewählten Uhrzeit, Speicherung in Variablen und Ausgabe im DOM
                el('#zeit').addEventListener('change',function(){
                    uhrzeit = this.options[this.selectedIndex].text;
                    zeit = this.options[this.selectedIndex].value;
                    // console.log('Name: ' + name);
                    el('#ausgabe_wahl').innerHTML += `<br> ${uhrzeit} Uhr`;
        

                    // Sitzplätze ensprechend der Kinosaalgröße (aus JSON) erstellen
                    sitzeErstellen(data);
                    
                    // gesperrte Sitzplätze erzeugen
                    blockSeats();

                    // Bild des ausgewählten Films auf der weißen Leinwand darstellen
                    loadImg(data,kino,film);
                    
                    // Hinweis anstelle des Select-Elements im DOM ausgeben
                    el('#zeit').remove();
                    el('#wahl').innerHTML = `<b>Wähle deine Sitzplätze.</b><br>Preis pro Sitzplatz: ${preis} €.` ;
                });
            });
        });
    };

    // Farbe ändern der ausgewählten Sitzplätze
    function colorSelectedSeats(){
        // console.log(this);
        let zustand = this.classList.value;
        let anzahlKlassen = this.classList.length;
        let dataNr = Number(this.getAttribute('data-nr'));
        // console.log(this.dataset.nr);
        // console.log(this.getAttribute('data-nr'));
        let seatsSelected = Array.from(group('#seats .selected'));      // erstellt NodeList aus allen HTML-Elementen, deren class 'selected' enthält, und wandelt die NodeList in ein Array um
        console.log(seatsSelected)

        let flag = true;        // steuert if-Bedingung
        
        // Bedingung, damit nur benachbarte Sitze ausgewählt werden können und der aktuelle Sitz selbst
        if(seatsSelected.length > 0){
            if(seatsSelected.find(item => Number(item.getAttribute('data-nr'))  === dataNr - 1) || seatsSelected.find(item => Number(item.getAttribute('data-nr'))  === dataNr + 1) || seatsSelected.find(item => Number(item.getAttribute('data-nr')) === dataNr)){
                flag = true;
            }else{
                fehlerMeldung('Sie dürfen nur benachbarte Sitze auswählen.')
                flag = false;
                return;
            };
        };
        
        // Sitze auswählen und class hinzufügen oder Sitze abwählen und class entfernen
        // flag verhindert das Löschen von Sitzplätzen innerhalb einer Reihe von ausgewählten Sitzplätzen
        // Ausgabe Fehlermeldung
        if(zustand === 'seat' && countSelected >= 5){
            fehlerMeldung('Sie haben die maxiamale Anzahl an Sitzen pro Kunde erreicht.')
        }else if(zustand === 'seat' && anzahlKlassen < 2 && flag){
            this.classList.add('selected');
            countSelected++;
        }else if(seatsSelected.find(item => Number(item.getAttribute('data-nr')) === dataNr - 1) && seatsSelected.find(item => Number(item.getAttribute('data-nr')) === dataNr + 1)){
            fehlerMeldung('Sie können keine Sitze innerhalb einer ausgewählt Reihe löschen.')
        }else{
            this.classList.remove('selected');
            countSelected--;
        };

        // Übergabe der Anzahl an ausgewählten Sitzen an die Variabel 'anzahl' zur weiteren verarbeitung in function preisAusgabe
        anzahl = Array.from(group('#seats .selected')).length;
        
        // Ausgabe Anzahl der Sitzplätze und Gesamtpreis imd DOM
        preisAusgabe(); 

    };

    // coronabedingte Abstände: sperren der Sitzplätze um belegte Sitzplätze herum
    function blockSeats(){

        // alle Sitzplätze aus DOM lesen und NodeList zu Array formen
        let allSeats = Array.from(group('#seats .seat'));
        // console.log(allSeats)

        // Schleife mit Prüfung belegter Sitzplätze und Zuweisung der class 'blocked'
        // Entfernen des EventListeners, damit Sitzplätze nicht mehr selektiert werden können
        allSeats.forEach(function(val,index){
            // console.log(val.classList.contains('occupied'));
            if(val.classList.contains('occupied')){
                let rowSeat = Number(val.getAttribute('data-row'));
                if(index >= 0 && index <=49)
                // je zwei Stühle werden links und rechts gesperrt
                for (let i = 1; i <= 2; i++){

                     
                    if(index - i >= 0){ // für die ersten beiden Sitzplätze in erster Reihe
                    // alle Sitzplätze links
                        if(allSeats[index - i].className === 'seat' && rowSeat == allSeats[index - i].getAttribute('data-row')){
                            allSeats[index - i].classList.add('blocked');
                            allSeats[index - i].removeEventListener('click', colorSelectedSeats);
                        };
                    };

                    if(index + i < allSeats.length){  // wenn letzter oder vorletzer Sitzplatz in letzter Reihe belegt ist
                        // alle Sitzplätze rechts
                        if(allSeats[index + i].className === 'seat' && rowSeat == allSeats[index + i].getAttribute('data-row')){
                            allSeats[index + i].classList.add('blocked');
                            allSeats[index + i].removeEventListener('click', colorSelectedSeats);
                        };
                    };
                };

                //  Reihen vor und hinter den belegten Sitzen sperren
                for(let i = 1; i < 10; i += 2){
                    if(group(`#seats .row [data-row="${rowSeat - i}"]`)){
                        let rowInFront = group(`#seats .row [data-row="${rowSeat - i}"]`);

                        rowInFront.forEach(function(val){  
                            if(!val.classList.contains('blocked') || !val.classList.contains('occupied')){            
                                val.classList.add('blocked');
                                val.removeEventListener('click', colorSelectedSeats);
                            };
                        });
                    };

                    if(group(`#seats .row [data-row="${rowSeat + i}"]`)){
                        let rowBehind = group(`#seats .row [data-row="${rowSeat + i}"]`); 
                        // console.log(rowBehind)

                        rowBehind.forEach(function(val){ 
                            // console.log(val)
                            if(!val.classList.contains('blocked') || !val.classList.contains('occupied')){            
                                val.classList.add('blocked');
                                val.removeEventListener('click', colorSelectedSeats);
                            };
                        });
                    };
                };
            };
        });

        // wenn alle Sitzplätze belegt sind
        let flag = allSeats.some(item => item.className === 'seat');
        // console.log(flag)

        if(!flag){
            fehlerMeldung('Für den ausgewählten Film sind alle Plätze belegt. Bitte wählen Sie eine andere Uhrzeit, ein anderes Kino oder einen anderen Film.')
            el('#okay').addEventListener('click',()=>{window.location.reload()}); // aktualisiert die Website und setzt alles auf Anfang
        };
    }; // ENDE blockSeats

    // erstellt select-Element im DOM mit allen Kinos aus der JSON-Datei
    function kinoAuswahl(data){
        el('.multi-container').innerHTML = ''       // div-Element leeren
        let keys = Object.keys(data);               // die properties aus dem JSON lesen
        // console.log(keys);

        // HTML-elemente erzeugen und mit attributen befüllen
        let label = create('label');                
        label.id = 'wahl';
        label.className = 'fade';
        label.innerHTML = 'Wähle ein:';

        let select = create('select');
        select.id = 'kino';
        select.className = 'fade';

        let option1 = create('option');
        option1.setAttribute('selected','')
        option1.setAttribute('disabled','')
        option1.innerHTML = "Kino"; 
        select.appendChild(option1);

        let option2 = create('option');
        option2.setAttribute('disabled','')
        option2.innerHTML = "──────────────";
        select.appendChild(option2);

        // alle Kinonamen aus JSON lesen und als Option ins Select-Element legen
        keys.forEach(function(val){
            // console.log(val)
            let option = create('option');
            option.value = val;
            option.innerHTML = data[val].name.name;
            // console.log(data[val].name.name)

            select.appendChild(option);
            
        });
        // console.log(select)
        el('.multi-container').appendChild(label);
        el('.multi-container').appendChild(select);

    }; // ENDE function kinoAuswahl


    function filmAuswahl(data,wert){

        el('.multi-container').innerHTML = '';       // div-Element leeren
        // let keys = Object.keys(data);               // die properties aus dem json lesen
        // // console.log(keys);

        // HTML-elemente erzeugen und mit attributen befüllen
        let label = create('label');                
        label.id = 'wahl';
        label.className = 'fade';
        label.innerHTML = 'Wähle einen:';

        let select = create('select');
        select.id = 'film';
        select.className = 'fade';


        let option1 = create('option');
        option1.setAttribute('selected','')
        option1.setAttribute('disabled','')
        option1.innerHTML = "Film"; 
        select.appendChild(option1);

        let option2 = create('option');
        option2.setAttribute('disabled','')
        option2.innerHTML = "─────────────";
        select.appendChild(option2);

        // alle Filmnamen aus JSON lesen und als Option ins Select-Element legen
        // console.log(data[wert].filme)
        data[wert].filme.forEach(function(val,index){
            // console.log(val);

            let option = create('option');
            option.value = index;  
            option.innerHTML = val.name;

            select.appendChild(option);
            
        });
        // console.log(select)
        el('.multi-container').appendChild(label);
        el('.multi-container').appendChild(select);

    }; // ENDE function filmAuswahl


    function zeitAuswahl(data,kino,film){

        preis = data[kino].filme[film].preis;
        
        el('.multi-container').innerHTML = ''       // div-Element leeren
        // let keys = Object.keys(data);               // die properties aus dem json lesen
        // // console.log(keys);

        // HTML-elemente erzeugen und mit attributen befüllen
        let label = create('label');                // HTML _element
        label.id = 'wahl';
        label.className = 'fade';
        label.innerHTML = 'Wähle eine:';

        let select = create('select');
        select.id = 'zeit';
        select.className = 'fade';


        let option1 = create('option');
        option1.setAttribute('selected','')
        option1.setAttribute('disabled','')
        option1.innerHTML = "Uhrzeit"; 
        select.appendChild(option1);

        let option2 = create('option');
        option2.setAttribute('disabled','')
        option2.innerHTML = "──────────────";
        select.appendChild(option2);

        // console.log(data[kino].filme[film].zeit)

        let arr = data[kino].filme[film].zeit;

        // alle Spielzeiten aus JSON lesen und als Option ins Select-Element legen
        arr.forEach(function(val,index){
            // console.log(val);

            let option = create('option');
            option.value = index;  
            option.innerHTML = val;

            select.appendChild(option);
            
        });
        // console.log(select)
        el('.multi-container').appendChild(label);
        el('.multi-container').appendChild(select);
    }; // ENDE function zeitAuswahl


    // Filmplakat(img) im Screen darstellen
    function loadImg(data){  
        el('.screen').innerHTML = '';    
        let img = new Image();
        // console.log(data[kino].filme[film].trailer_src, `kino: ${kino} film: ${film}`)
        img.setAttribute('src',data[kino].filme[film].trailer_src);
        el('.screen').appendChild(img);
    };

    // Sitzplätze nach Informationen aus JSON-Datei erzeugen und mit reservierten Sitzplätzen aus dem Local Storage abgleichen
    function sitzeErstellen(data){
        
        // Reservierung der Sitzplätze auslesen entsprechend ausgewähltem Kino, Film und Uhrzeit
        let res = readStorage();
        // console.log(res);

        el('#seats').innerHTML = '' // alte Sitzplätze werden aus DOM entfernt

        let wrapper = create('div');    // wrapper sammelt alle HTML-Elemente, bevor der Wrapper ins DOM eingesetzt wird

        let counter = 0;

        // 1.Schleife für Reihen 
        for (let i = 0; i < data[kino].filme[film].reihen; i++ ){
            // console.log(i);
            let row = create('div');
            row.className = 'row';
            row.setAttribute('data-row',i)

            // 2.Schleife für Sitzplätze
            for(let ii = 0; ii < data[kino].filme[film].sitze; ii++){
                counter++;


                let seat = create('div');
                seat.id =`s${counter}`;
                seat.setAttribute('data-nr',`${counter}`);
                seat.setAttribute('data-row',i)

                // console.log(kino === res.kino)

                // Reservierung mit class 'occupied' den entsprechenden Sitzplätzen zuweisen
                if(kino === res.kino && film === res.film && zeit === res.zeit && res.seats[seat.getAttribute('data-nr')]){

                    seat.className = 'seat occupied';

                }else{
                    seat.className = 'seat';
                    seat.addEventListener('click', colorSelectedSeats);
                };


                row.appendChild(seat);

            }; // ENDE Schleife für Sitze

            wrapper.appendChild(row);
        }; // ENDE Schleife für Reihen

        // alle Sitzplätze und Reihen ins DOM einfügen
        el('#seats').appendChild(wrapper);
    };

    // Ausgabe Anzahl ausgewählter Sitzplätze und Gesamtpreis
    function preisAusgabe(){
        el('#count').innerHTML = anzahl;
        el('#total').innerHTML = anzahl * preis;
    };

    //####################### Speichern und Lesen ####################################
    function saveStorage(){
        
        // Objekt mit Name Kino, Name Film, Uhrzeit film und selected Stühle
        
        let key = 'res' + kino + film + zeit;
        let res = {};
            res.kino = kino;
            res.film = film;
            res.zeit = zeit;
            res.seats = {};
        
            let sitze = group('#seats .seat');
        
            // Schleife: ausgewälte Sitzplätze = true, freie Sitzplätze = false 
            sitze.forEach(function(val){
                // console.log(val.classList);
                if(val.classList.contains('selected') || val.classList.contains('occupied')){
                    res.seats[val.getAttribute('data-nr')] = true;
                }else{
                    res.seats[val.getAttribute('data-nr')] = false;
                }; // ENDE if

            });
        // console.log(res)
        // Objekt mit reservierten Sitzen im Local Storage speichern
        storage.save(key,res);


        // Generieren des AbholCodes und speichern in localStorage in seperatem Objekt
        
        let keyCode = 'reservierungen'
        let resSitze = [];

        list = storage.read(keyCode);
        // console.log(list)
       
        let resCode = abholCode();
        // console.log(typeof abholCode());

        resSitze.push(nameKino);
        resSitze.push(nameFilm);
        resSitze.push(uhrzeit);


        sitze.forEach(function(val){
            // console.log(val.classList);
            if(val.classList.contains('selected')){
                resSitze.push(val.id);
            }; // ENDE if
        });


        list[resCode] = resSitze;
        // console.log(resCode)
        // Objekt mit Code, Kinoname, Filmname, Uhrzeit und reservierten Sitzplätz-Nr.
        storage.save(keyCode,list);

        return resCode;
    };


    // Objekt nach ausgewähltem Kino, Film und Uhrzeit aus Local Storage lesen
    function readStorage(){

        
        let res = storage.read('res' + kino + film + zeit);

        // console.log(res);

        return res;
        
    };

    //################## ENDE Speichern und Lesen ####################################


    // viertstelliger Code mit einem Buchstaben
    function abholCode(){
        let code;
        let buchstaben ='ABCDEFGHJKLMNPQRSTUVWXYZ'
        
        // wählt zufällig EINEN Buchstaben aus varibale 'buchstaben' aus
        code = buchstaben.charAt(Math.floor(Math.random() * buchstaben.length));

        // wählt nacheinander VIER zufällige Zahlen zwischen 0 und 9
        for(let i = 0; i < 4; i++){
            code += Math.floor(Math.random() * 9);
        };

        // Obwohl code vom TypeOf String ist, wird dieser in der Funktion saveStorage nicht verarbeitet. 
        // die Anwendnung der Methode toString() oder das Setzen von `${code}` behebt das Problem  
        return code.toString();
    };

    // Funktion weist darauf hin, dass erst ein Kino und Film ausgewählt werden muss, bevor Sitze selektiert werden können
    function error(){

        let arr = group('#seats .seat');
        // console.log(arr)

        arr.forEach(val =>{
            val.addEventListener('click', function(){
                fehlerMeldung('Wähle erst ein Kino, einen Film und eine Uhrzeit aus.')
            });
            // console.log(val)
        });

    };

    // Ausgabe von Fehlermeldungen als PopUp; Text wird als Parameter übergeben
    function fehlerMeldung(txt){

        el('#alert p').innerHTML = txt;
        el('#alert').className = "alert_aktiv";

        el('#okay').addEventListener('click',function(){

            el('#alert').className = "alert_passiv";

        });
    };

    //######## Abteilung Canvas #################################
    //gefunden im Internet und angepasst auf unsere Bedürfnisse
    //############################################################

    // Reglerausgabe: Personen in Bewegung
    el('#num').addEventListener('input', function(){
        el('#value').innerHTML = el('#num').value;
    });


    // definiern eine class Person für 50 Personsen im CanvasCorona
    class Person {
        constructor(status) {
            this.x = 0;
            this.y = 0;
            this.r = 6;
            this.v = 0;
            this.angle = 0;
            this.status = status;
        }
        move() {
            // Bewegungsraum im Canvas festlegen
            //  console.log("move");
            this.x += this.v * Math.cos(this.angle);// gibt den Cosinus eines Winkels zurück für x
            this.y += this.v * Math.sin(this.angle);// gibt den Cininus eines Winkels zurück für y
            if((this.x < this.r) || (this.x > canvasCorona.width - this.r)) {
                this.angle = Math.PI - this.angle;

                if(this.x < this.r) {
                    this.x = this.r;
                };

                if(this.x > canvasCorona.width - this.r) {
                    this.x = canvasCorona.width - this.r;
                };
            };

            if((this.y < this.r) || (this.y > canvasCorona.height - this.r)) {

                this.angle = Math.PI*2 - this.angle;

                if(this.y < this.r) {
                    this.y = this.r;
                };
                if(this.y > canvasCorona.height - this.r) {
                    this.y = canvasCorona.height - this.r;
                };
            };
        };
        kollision(target) {   // Kollisionsabfrage

            let dx = target.x - this.x;
            let dy = target.y - this.y;
            let d  = (dx ** 2 + dy ** 2) ** 0.5;
            if(d < this.r * 2){
                if(this.status == "positive") {
                    target.status = "positive";
                };
                if(target.status == "positive") {
                    this.status = "positive";
                };
                target.angle = Math.atan2(dy,dx);   // eine methode in mathe geben zurück the angle zwichen x-axis und the ray von(0,0)zu point (dx,dy)
                this.angle = Math.PI - this.angle + target.angle *2;
                if((this.v > 0) && (target.v > 0)) {
                    this.v = (target.v + this.v)/2;
                    target.v = this.v;
                };
                this.move();
                target.move();
            };
        };
        draw() {
            ctx.strokeStyle = "#666666";        //color der Rahmen Gray
            ctx.fillStyle = "#ffffff";          // person mit farbe weiß
            if (this.status == "positive") {    // Änderung zur Farbe Rot
                ctx.fillStyle = "#FF4500";
            };
            if (this.status == "positive") {     //änderung zur Farbe Rot
                ctx.fillStyle = "#FF4500";
            };
            ctx.beginPath();                    //erstellen einen Pfad und beendet ggf.
            ctx.arc(this.x,this.y,this.r,0,Math.PI*2);// zeichet  Kreise mit Pfad und Radius.
            ctx.fill();                         //der Pfad beendet.
            ctx.stroke();                       //zeichnet die Kontur des Pfades.
        };
    };

    function initCanvas() {

        canvasCorona = el("#cinema");               // definieren Canvas für personen 
        ctx = canvasCorona.getContext("2d");

        graphCanvas = el("#graph");                 // definieren canvas für chart
        graphCtx = graphCanvas.getContext("2d");

        ctx.clearRect(0,0,canvasCorona.width,canvasCorona.height);  // Canvas pixel leeren
        ctx.drawImage(imgCanvasCorona,0,0);                 // Hintergrundbild hinzufügen canvas2
        
        graph = [];
        graphCtx.clearRect(0,0,graphCanvas.width,graphCanvas.height);
        graphCtx.drawImage(imgCanvasGraph,0,0);             // Hintergrundbild hinzufügen
        graphCtx.strokeStyle = "#6666FF";
        graphCtx.beginPath();
        graphCtx.moveTo(0,graphCanvas.height/2);
        graphCtx.stroke();                          //zeichnet die Kontur des Pfades für canvas graph
        persons = [];                               // leer array von personen am anfang
        let num = el("#num").value;


        // eine for schleife für 50 personen in canvas city
        for (let i = 0; i < 50; i++) {  
            let person = new Person("negative");
            if (i == 0) {
                person.status = "positive";
            };
            if (i < num) {
                person.v = 1;
                person.angle = Math.random() * Math.PI+2;
            };
            persons.push(person);// array persons mit 50 object person
            // console.log(persons)
        };

        for (let i = persons.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random()*i);
            [persons[i],persons[j]] = [persons[j],persons[i]];
        };

        for (let y = 0; y < 5; y++) {
            for (let x = 0; x < 10; x++) {
                let index = x + y * 10;
                persons[index].x = x * 42 + 21;
                persons[index].y = y * 42 + 21;
                persons[index].draw();
            };
        };
    };
    // function um Simulation erneut zu starten
    function startSimulation(){
        initCanvas();
        elapsedTime = 0;
        if (animate) {
            cancelAnimationFrame(animate);
        };
        render();
        // console.log(animate)
    };

    function render () {
       
       animate = requestAnimationFrame(render);
        
        for (let person of persons) {
            person.move();

            for (let target of persons) {
                if(person != target){
                    person.kollision(target);
                }
            };
        };

        let cnt = 0;
        ctx.clearRect(0,0,canvasCorona.width,canvasCorona.height);   // Canvas pixel leeren
        ctx.drawImage(imgCanvasCorona,0,0);                          // Hintergrundbild hinzufügen

        // Personen im Canvas animieren/bewegen/zeichnen
        for (let person of persons) {
            person.draw();

            if(person.status == "positive"){
                cnt++;
            };
        };
        graph.push(cnt);
        // console.log(cnt);
        // console.log(graph)


        // Graphen im Canvas zeichnen
        graphCtx.clearRect(0,0,graphCanvas.width,graphCanvas.height);
        graphCtx.drawImage(imgCanvasGraph,0,0);
        

        // Graphen im Canvas zeichnen
        graph.forEach(function(val,index){
            graphCtx.strokeStyle = "#FF4500";
            graphCtx.lineWidth = 2;
            graphCtx.beginPath();
            graphCtx.moveTo(index,graphCanvas.height);
            graphCtx.lineTo(index,graphCanvas.height - (val * 1.4));
            graphCtx.stroke();

            // console.log(val)
        });

        // graphCtx.lineWidth = 1;
        // graphCtx.beginPath();
        // graphCtx.moveTo(0,graphCanvas.height - (cnt * 1.4));
        // graphCtx.lineTo(elapsedTime,graphCanvas.height - (cnt * 1.4));
        // graphCtx.stroke();


        // filledRect und Kreis in Canvas
        graphCtx.fillStyle = "#FF4500";
        graphCtx.strokeStyle = "#FFFFFF";                         
       
        graphCtx.fillRect(45, 18, 100, 34);
        graphCtx.strokeRect(45, 18, 100, 34);

        graphCtx.beginPath();                    
        graphCtx.arc(35, graphCanvas.height/2,24,0,Math.PI*2);
        graphCtx.fill();
        graphCtx.stroke();          
        

        
        // Text in Canvas: Zahl der infizierten Personen
        graphCtx.font = "30px Helvetica";
        graphCtx.fillStyle = "#FFFFFF";
        graphCtx.textAlign = "center";
        graphCtx.fillText(cnt-1, 35, graphCanvas.height/2 + 10);
        
        // Text in Canvas: Infizierte
        graphCtx.font = "20px Helvetica";
        graphCtx.fillStyle = "#FFFFFF";
        // graphCtx.textAlign = "center";
        graphCtx.fillText('infiziert', 100, graphCanvas.height/2 + 7);
        
        
        elapsedTime++;  // rundenzähler

        // Simulation wird automatisch gestoppt, 
        // wenn der Graph am rechten Rand seines Canvas angekommen ist;
        // Breite Canvas (graphCanvas.width) 420 px
        if(elapsedTime > graphCanvas.width) { 
            cancelAnimationFrame(animate);
            animate = false;
        };
    };

    //****************************************************************/
    //******************** Zuweisen und Ausführen ********************/
    //****************************************************************/


    // Auswahl-Bestätigen-Button
    el('#btn').addEventListener('click', function(){

        if(anzahl === 0){
            if(zeit){
                fehlerMeldung('Sie haben keine Plätze ausgewählt.')
            }else{
                fehlerMeldung('Wähle erst ein Kino, einen Film und eine Uhrzeit aus.')
            };

        }else{
            // console.log(kino, film, zeit)
            let code = saveStorage();

            // Popup "Ihre Reserveriung" befüllen
            //  id = "platz">
            if(anzahl > 1){
                el('#platz').innerHTML = `${anzahl} Plätze`;
            }else{
                el('#platz').innerHTML = `${anzahl} Platz`;
            };
            
            //  id = "filmname"> 
            el('#filmname').innerHTML = nameFilm;

            //  id = "zeit"
            el('#zeit').innerHTML = uhrzeit + ' Uhr';

            //  id = "kinoname">
            el('#kinoname').innerHTML = nameKino;

            //  id = "price">
            el('#price').innerHTML = anzahl * preis + ' €';
            
            el('#code').innerHTML = code;
            // console.log(code)

            // Popup "Ihre Reserveriung" anzeigen
            el('#final').className = 'final_aktiv';

            // Canvas-Simulation autostart
            startSimulation();
        };


    });

    // Server-Löschen-Button
    el('#clear').addEventListener('click',function(){

        // WARNUNG
            el('#alert h3').innerHTML = 'Achtung!';
            el('#alert p').innerHTML = 'Diese Funktion steht nur Benutzern mit Admin-Rechten zur Verfügung. Sind Sie sicher, alle Reservierungsdaten vom Server löschen zu wollen?';
            el('#okay').innerHTML = 'Ich bin mir sicher!'
            el('#alert').className = "alert_aktiv";
    
            el('#okay').addEventListener('click',function(){
    
                el('#alert').className = "alert_passiv";
                el('#alert h3').innerHTML = 'Fehler';
                storage.clear();
                window.location.reload(); // aktualisiert die  Website und setzt alles auf anfang
            });
    });

    // PopUp schließen
    el('#close').addEventListener('click',function(){
        window.location.reload(); // aktualisiert die  Website und setzt alles auf anfang
        el('#final').className = 'final_passiv';
        
    });

    // Canvas erst starten, wenn Hintergrundbilder geladen worden sind (Test)
    // imgCanvasCorona.addEventListener('load',initCanvas);

    // Neustart-Button der Simulation im PopUp
    el('#startSim').addEventListener('click', startSimulation)
    

    loadJson();
    error();
}());